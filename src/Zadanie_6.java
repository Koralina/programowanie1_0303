import java.math.BigDecimal;
import java.util.Scanner;

public class Zadanie_6 {
    public void profit () {
        Scanner input = new Scanner (System.in);
        System.out.println("How much would you like to invest?");
        float investment = input.nextFloat();
        System.out.println("What is the interest rate on the investment?");
        float percent = input.nextFloat();
        System.out.println("How long will the investment take(in years)? ");
        float time = input.nextFloat();
        float profit = ((investment * percent)*0.01F)*time;
        System.out.println("Your profit: " + profit);
        profitA();
    }
    public void profitA () {
        int period;
        Scanner input = new Scanner (System.in);
        System.out.println("This method will help you calculate your profit from the investment depending on daily, weekly, monthly or per annum interest rate ");
        System.out.println("How much would you like to invest?");
        float investment = input.nextFloat();
        System.out.println("What is the interest rate on the investment?");
        float percent = input.nextFloat();
        System.out.println("Is the interest rate on daily (1), weekly (2), monthly basis (3) or per annum (4)? Enter correct number ");
        period = input.nextInt();

        switch (period) {
            case 1:
                System.out.println("How long will the investment take(in days)? ");
                int days = input.nextInt();
                float profit1 = ((investment * percent)*0.01F)*days;
                System.out.println("Your profit is: " + profit1);
                break;
            case 2:
                System.out.println("How long will the investment take(in weeks)? ");
                int weeks = input.nextInt();
                float profit2 = ((investment * percent)*0.01F)*weeks;
                System.out.println("Your profit is: " + profit2);
                break;
            case 3:
                System.out.println("How long will the investment take(in months)? ");
                int months = input.nextInt();
                float profit3 = ((investment * percent)*0.01F)*months;
                System.out.println("Your profit is: " + profit3);
                break;
            case 4:
                System.out.println("How long will the investment take(in years)? ");
                int years = input.nextInt();
                float profit4 = ((investment * percent)*0.01F)*years;
                System.out.println("Your profit is: " + profit4);
                break;
        }
    }
}