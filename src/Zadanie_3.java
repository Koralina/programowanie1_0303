import java.util.Arrays;

public class Zadanie_3 {
    public int[] tablica;

    public Zadanie_3() {
    }

    public void wypelnijTablice() {
        tablica = new int[25];
        for (int i = 0; i < 25; i++) {
            tablica[i] = i;
        }
    }

    public void wydrukujWszystkie() {                                                       // podpunkt a) wypisanie wszystkich argumentów tablicy
        System.out.println("To są wszystkie argumenty " + Arrays.toString(tablica));
    }

    public void wydrukujOdKonca() {                                                         // podpunkt b) wydrukowanie wszystkich argumentów od końca
        int odKonca[] = new int[tablica.length];
        int j = tablica.length - 1;
        for (int i = 0; i < 25; i++) {
            odKonca[i] = tablica[j];
            j--;
        }
        System.out.println("To są argumety wydrukowane od końca " + Arrays.toString(odKonca));

    }

    public void printNaNieparzystychPoz() {                                         // podpunkt c) print liczb na nieparzystych pozycjach tablicy
        tablica[3] = 258;                                                           // w pozycji 3 tablicy, zamieniam liczbę 3 na 258, bo tak
        System.out.print("To są argumenty na nieparzystych pozycjach tablicy: ");
        for (int i = 0; i < tablica.length; i++) {                                 // przechodze po tablicy
            if (i % 2 == 1) {                                                       // if- jesli pozycja danej liczby w tabeli jest nieparzysta(odd)    PARZYSTE-EVEN
                System.out.print(tablica[i] + " ");                                 // to drukuje te argumenty tabeli rozdzielajac je spacjami
            }
        }
    }

    public void podzielnePrzez3() {                                     // podpunkt d) print argumentów podzielnych przez 3
        System.out.print("\n" + "To są liczby podzielne przez 3: ");  // na początku printa wrzucam \n, bo nie umiem w mniej skomplikowany sposób odzielić  wydruku z podpunktu c)
        for (int i = 0; i < tablica.length; i++) {                      // przechodzę po tablicy
            if (tablica[i] % 3 == 0) {                                  // if- jesli mijany argument tablicy jest podzielny przez 3 (modulo --> reszta z dzielenia przez 3 daje 0)
                System.out.print(tablica[i] + " ");                     // to wydrukuj dany argument
            }
        }
        System.out.print("\n");
    }

    public void sumAll() {                                                                        // subpoint e) sum up all table arguments
        int suma = 0;                                                                            // declare the variable that will be used for printing
        System.out.println("To są aktualnie sumowane argumenty: " + Arrays.toString(tablica));   // current table arguments
        System.out.print("To jest suma wszystkich argumentów tablicy: ");
        for (int i = 0; i < tablica.length; i++) {                                               //go through the table
            suma += tablica[i];                                                                  // += sum up table arguments
        }
        System.out.println(suma);                                                                // print the sum
    }


    public void sumFirst4() {                                                               //subpoint f) sum up the first four arguments of the table
        int suma = 0;                                                                       // declare variable that wille be used for summing and printing
        System.out.print("To jest suma pierwszych czterech argumentów tablicy: ");
        for (int i = 0; i < 4; i++) {                                                        // going through the table
            suma += tablica[i];                                                              // += summing up table arguments
        }
        System.out.println(suma);                                                           // printing
    }

    public void sumLast5() {                                // subpoint g) sum up the last 5 arguments of the table that are bigger than 2
        int suma = 0;
        tablica[23] = 1;                                   // changing table argument at the 23 position
        System.out.println("Sprawdzam co mam aktualnie w tablicy: " + Arrays.toString(tablica));        // checking table arguments
        for (int i = tablica.length - 5; i < 25; i++) {
            if (tablica[i] > 2)
                suma += tablica[i];
        }
        System.out.println("To jest suma ostatnich 5 argumentów tablicy, które są większe od 2: " + suma);
    }

    public void sumUpTo10() {                // subpoint h) show how many first arguments of the table should be summed up to get the number that is bigger than 10
        int suma = 0;                       // declare the suma variable
        int iloscZsumowanych = 0;           //declare the variable that is the counter of summed arguments
        tablica[3] = 2;                     //changing argument at the 3 table position

        for (int i = 0; i < tablica.length; i++) {  //going through the table
            while (suma <= 10) {                    //requirement for the loop to be activated || still go on
                suma = suma + tablica[i];           //summing && || adding next arguments to the previous sum
                i++;                                //counter for the table arguments
                iloscZsumowanych++;                 // new counter for getting the number of arguments
            }

        }
        System.out.println("To są aktualne argumenty tablicy: " + Arrays.toString(tablica));
        System.out.println("To jest suma kolejnych argumentów do momentu uzyskania liczby większej od 10: " + suma);
        System.out.println("To jest ilość zsumowanych argumentów w w/w działaniu: " + iloscZsumowanych);
    }

}
