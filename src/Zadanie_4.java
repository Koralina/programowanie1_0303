import java.util.Scanner;

public class Zadanie_4 {

    public void maxMin() {

        Scanner input = new Scanner(System.in);             //declare Scanner
        System.out.println("Podaj liczbę całkowitą ");
        int liczba1 = input.nextInt();                  //get number from the user and assign int to liczba1
        System.out.println("Podaj liczbę całkowitą ");
        int liczba2 = input.nextInt();
        System.out.println("Podaj liczbę całkowitą");
        int liczba3 = input.nextInt();

        if (liczba1 >= liczba2 && liczba1 >= liczba3) {
            System.out.println("Największą liczbą jest " + liczba1);
        } else if (liczba2 >= liczba1 && liczba2 >= liczba3) {
            System.out.println("Największą liczbą jest " + liczba2);
        } else {
            System.out.println("Największą liczbą jest " + liczba3);
        }
    }
}