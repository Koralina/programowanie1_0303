import java.util.Scanner;

public class Zadanie_5 {
    public void meanSum() {
        Scanner wejscie = new Scanner(System.in);
        System.out.println("Podaj liczbę ");
        float liczba1 = wejscie.nextFloat();
        System.out.println("Podaj liczbę ");
        float liczba2 = wejscie.nextFloat();
        System.out.println("Podaj liczbę ");
        float liczba3 = wejscie.nextFloat();

        float suma = liczba1 + liczba2 + liczba3;
        System.out.println("Suma podanych liczb: " + suma);

        float mean = (liczba1 + liczba2 + liczba3) / 3;
        System.out.println("Średnia podanych liczb: " + mean);
    }
}
