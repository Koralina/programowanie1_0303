public class Zadanie_1 {
    int wiek;

    public Zadanie_1(int wiek) {
        this.wiek = wiek;

        if (wiek < 18) {
            System.out.println("Osoba niepełnoletnia");
        } else {
            System.out.println("Osoba pełnoletnia");
        }
    }
}